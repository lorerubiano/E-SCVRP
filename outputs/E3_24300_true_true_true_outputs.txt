***************************************************
*                      OUTPUTS                    *
***************************************************


--------------------------------------------

 OUR BEST SOLUTION:

--------------------------------------------

Sol ID : 8046551
Sol Optimization costs: 985.6865055757848
Sol Total costs: 985.6865055757848
Sol Dist 828.9219360351562
Sol Dist cost: 828.9219360351562
Sol Time: 13.300000190734863
Sol Time cost: 131.85299682617188
Sol Social cost: 24.91156768798828
Sol largest route (Time): 11.769999999999998
Sol shortest route (Time): 4.52
Sol largest route (Dist): 700.8557114684778
Sol shortest route (Dist): 281.72213056466745
Sol computing time: 0.001070737
# of routes in sol: 5


List of routes (cost and nodes): 

Route 0 || Total costs = 234.26434337939563 || Optimization costs = 234.26434337939563 || Social Cost = 5.984207608409168 || Distance  = 197.84643577098643 || Distance Cost = 197.84643577098643 || Time  = 3.0700000000000003 || Time cost  = 30.433700000000005 || Demand  = 158.0 || Nodes  = 0-27-31-28-3-22-1-10-45-44-42-19-40-41-0
Route 1 || Total costs = 177.28327512652257 || Optimization costs = 177.28327512652257 || Social Cost = 3.6142053715567726 || Distance  = 149.7759697549658 || Distance Cost = 149.7759697549658 || Time  = 2.4099999999999997 || Time cost  = 23.8931 || Demand  = 149.0 || Nodes  = 0-18-13-4-2-20-36-32-0
Route 2 || Total costs = 171.77233571156745 || Optimization costs = 171.77233571156745 || Social Cost = 4.473123700397664 || Distance  = 143.9016120111698 || Distance Cost = 143.9016120111698 || Time  = 2.36 || Time cost  = 23.3976 || Demand  = 156.0 || Nodes  = 0-48-8-26-7-23-17-37-15-33-12-0
Route 3 || Total costs = 151.47631684098192 || Optimization costs = 151.47631684098192 || Social Cost = 4.269618755494232 || Distance  = 126.68299808548767 || Distance Cost = 126.68299808548767 || Time  = 2.0700000000000003 || Time cost  = 20.523700000000005 || Demand  = 156.0 || Nodes  = 0-14-25-49-9-34-21-50-16-11-0
Route 4 || Total costs = 250.8902345173173 || Optimization costs = 250.8902345173173 || Social Cost = 6.570411383872612 || Distance  = 210.7149231334447 || Distance Cost = 210.7149231334447 || Time  = 3.3900000000000006 || Time cost  = 33.6049 || Demand  = 158.0 || Nodes  = 0-6-43-24-47-5-39-30-35-29-38-46-0


