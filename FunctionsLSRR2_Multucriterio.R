library(stringr)
library(reshape2)

readRoutes<-function(file){
  fil<-readLines(file)
  long<-length(fil)
  lj<-grep(pattern="Nodes", x=fil)
  routes<-as.vector(length(lj))
  r<-length(lj)
  routes<-NULL
  for(j in 1:r){
    rj<-fil[lj[j]]
    ind<-str_locate_all(rj,"Nodes")
    routes[j]<-str_sub(rj,ind[[1]][1,1]+9)
    }
    d<-NULL 
    for(l in 1:length(routes)){
    f<-as.numeric(unlist(strsplit(routes[l],"-")))
    r<-seq(0,length=length(f))
    for(a in 1:length(f)){
    r[a]<-as.numeric(unlist(f[a]))}
    d[[l]]<-r}
  return(d)}

getDist<-function(ab){
  total<-dim(ab)[1]
  d<-matrix(0,total,total)
  for(i in 1:total){
    for(j in i:total){
      d[i,j]<-sqrt(sum((ab[i,1:2]-ab[j,1:2])^2))
      d[j,i]<-d[i,j]}}
  return(d)}


getTimes<-function(infoTime){
  infoTime<-cbind(infoTime,rep(0,dim(infoTime)[1]))
  for(i in 1:dim(infoTime)[1]){
    infoTime[i,4]<-infoTime[i,3]}
  return(infoTime)}

computeDist<-function(route,dists){
  d<-0
  for(i in 1:(length(route)-1)){
    d<-d+dists[route[i]+1,route[i+1]+1]}
  return (d)}

computeTimes<-function(route,infoTime){
  t<-0
  for(i in 1:(length(route)-1)){
    a<-route[i]
    b<-route[i+1]
    if(a<b) t<-t+infoTime[infoTime[,1]==a & infoTime[,2]==b,4]
    else t<-t+infoTime[infoTime[,1]==b & infoTime[,2]==a,4]}
  return (t)}

computeEmissions<-function(route,dists,kpl){
  emissions<-0
  for(i in 1:(length(route)-1)){
    emissions<-emissions+dists[route[i],route[i+1]]*kpl}
  return (emissions)}

computeSocial<-function(route,dists,caSocial,infoClientes){
  ddaTotal<-0
  ddaAcumulada<-rep(0,(length(route)-1))
  for(i in 2:(length(route)-1)){
    ddaTotal<-as.numeric(ddaTotal+infoClientes[route[i]+1,3])
    if(i==2){ddaAcumulada[i-1]<-infoClientes[route[i]+1,3]}
    else{ddaAcumulada[i-1]<-ddaAcumulada[i-2]+infoClientes[route[i]+1,3]}}
  coste<-0
  for(i in 1:(length(route)-1)){
    if(i==1){coste<-dists[route[i]+1,route[i+1]+1]*(ddaTotal)}
    else{coste<-coste+dists[route[i]+1,route[i+1]+1]*(ddaTotal-ddaAcumulada[i-1])}}
  return (coste)}

computeCostDist<-function(totalDist,caDistance,kpl){
  return(totalDist*caDistance*kpl)}

computeCostTime<-function(totalTime,DW,FC){
  return((totalTime*DW)+FC)}

computeCostSocial<-function(totalSocial,caSocial){
  return(totalSocial*caSocial)}

computeoptCost<-function(costDist,costTime,costSocial,importanceWeight){
  return(sum(importanceWeight[[2]]*costDist,importanceWeight[[3]]*costTime,importanceWeight[[1]]*costSocial))}


computeTotalCost<-function(costDist,costTime,costSocial){
  return(sum(costDist,costTime,costSocial))}


checkAllCustomerServed<-function(routes,infoClientes){
  n<-dim(infoClientes)[1]
  n2<-sum(0:n %in% unique(unlist(routes)))
  return (n==n2)}

checkMaxTimeBattery<-function(routes,maxTime,infoTime){
  l<-length(routes)
  l2<-NULL
  for(i in 1:l){
    aux<-computeTimes(routes[[i]],infoTime)
    l2[i]<-(aux<=maxTime)}
  l3<-sum(l2)
  return (l==l3)}

checkMaxTimeWorkingHours<-function(routes,maxTime,infoTime){
  l<-length(routes)
  l2<-NULL
  for(i in 1:l){
    aux<-computeTimes(routes[[i]],infoTime)
    l2[i]<-(aux<=maxTime)}
  l3<-sum(l2)
  return (l==l3)}

checkMaxDemand<-function(routes,capacity,infoClientes){
  l<-length(routes)
  l2<-NULL
  for(j in 1:l){
    aux<-0
    for(i in 1:(length(routes[[j]])-1)){
      aux<-aux+infoClientes[routes[[j]][i]+1,3]}
    l2[j]<-(aux<=capacity)}
  l3<-sum(l2)
  return (l==l3)}

