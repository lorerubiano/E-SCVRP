import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class TestsManager {
    public static ArrayList<Test> getTestsList(String testsFilePath) {
        ArrayList<Test> list = new ArrayList<Test>();
        try {
            FileReader reader = new FileReader(testsFilePath);
            Scanner in = new Scanner(reader);
            in.useLocale(Locale.US);
            // The two first lines (lines 0 and 1) of this file are like this:
            //# instance | maxRouteCosts | serviceCosts | maxTime(sec) | ...
            // A-n32-k5       10000000          0              120       ...
            while(in.hasNextLine()) {
                String s = in.next();
                if(s.charAt(0) == '#') // this is a comment line
                    in.nextLine(); // skip comment lines
                else {
                    String instanceName = s; // e.g.: A-n32-k5
                    float maxRouteCosts = in.nextFloat(); // maxCosts per route
                    float maxTime = in.nextFloat(); // max computational time (sec)
                    String distrib = in.next(); // statistical distribution
                    float firstParam = in.nextFloat(); // geometric distribution parameter
                    float secondParam = in.nextFloat(); // geometric distribution parameter
                    float maxP = in.nextFloat(); // max percentage to remove routes
                    float medP = in.nextFloat(); // med percentage to remove routes
                    float minP = in.nextFloat(); // min percentage to remove routes
                    int seed = in.nextInt(); // seed for the RNG
                    boolean iTime = in.nextBoolean();
                    boolean iDistance = in.nextBoolean();
                    boolean iSocial = in.nextBoolean();
                    Test aTest = new Test(instanceName, maxRouteCosts, maxTime, distrib, firstParam, secondParam, maxP, medP, minP, seed, iTime, iDistance, iSocial);
                    list.add(aTest);}}
            in.close();}
        catch (IOException exception) {
            System.out.println("Error processing tests file: " + exception);}
        return list;}}