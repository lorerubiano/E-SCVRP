import java.util.LinkedList;
import java.util.Random;

/**
 * Iteratively calls the RandCWS and saves the best solution.
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class IGAlgorithm 
{
    /* 0. Instance fields and class constructor */
    private Test aTest;
    private Inputs inputs; // contains savingsList too
    private Random rng;
    private Costs costList;
    private Solution bestSol = null;
    private Solution newSol = null;
    private Outputs outputs = new Outputs();
    private RouteCache cache;
        
    IGAlgorithm(Test myTest, Inputs myInputs, Random myRng, Costs myCosts)
    {
        aTest = myTest;
        inputs = myInputs;
        rng = myRng;
        costList=myCosts;
        cache = new RouteCache();}
   
    public Outputs solve()
    {
        /* 1. Generates the CWS solution */
        long start = ElapsedTime.systemTime();
        /* 3. Iterates calls to RandCWS */
	    double elapsed = 0.0;
	    bestSol = RandCWS.solve(aTest, inputs, rng, costList, false);
        elapsed = ElapsedTime.calcElapsed(start, ElapsedTime.systemTime());
        bestSol.LocalSearch(aTest, inputs, rng, costList, true);
        bestSol.setElapsedTime(elapsed);
	    while(elapsed < aTest.getMaxTime()){
	        newSol = VNS.ShakeSolution(bestSol,costList,rng,aTest, inputs);
	        newSol.fastLocalSearch(inputs, cache,aTest,costList);
	        newSol.LocalSearch(aTest, inputs, rng, costList, true);
	        if(newSol.getOptimizationCost() < bestSol.getOptimizationCost()) 
	        	{
	        	 	elapsed = ElapsedTime.calcElapsed(start, ElapsedTime.systemTime());
	        	 	newSol.setElapsedTime(elapsed);
	        	    bestSol = newSol;
	        		System.out.println("bestSol Optimization cost: " + newSol.getOptimizationCost());
	        	}
	        bestSol.LocalSearch(aTest, inputs, rng, costList, true);
	            elapsed = ElapsedTime.calcElapsed(start, ElapsedTime.systemTime());
	        }
	        System.out.println("OBSol Optimization cost: " + bestSol.getOptimizationCost());
	        System.out.println("OBSol Total cost: " + bestSol.getTotalCost());
	        System.out.println("OBSol demand: " + bestSol.getDemand());
	        System.out.println("OBSol time: " + bestSol.getTime());
	        System.out.println("OBSol routes: " + bestSol.getRoutes().size());
	        bestSol.updateSolution(costList, aTest);
	        outputs.setOBSol(bestSol);
	        /* 4. Returns the CWS sol. and the best-found sol. */
	        return outputs;}

	
}