import java.io.File;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class IGTester
{
    final static String inputFolder = "inputs";
    final static String outputFolder = "outputs";
    final static String testFolder = "tests";
    final static String fileNameTest = "test2run.txt";
    final static String sufixFileNodes = "_nodes.txt";
    final static String sufixFileTimes = "_medium.txt";
    final static String sufixFileVehicules = "_vehicles.txt";
    final static String sufixFileOutput = "_outputs.txt";
    final static String sufixFileParameter = "Parameters.txt";

    public static void main( String[] args )
    {
        System.out.println("****  WELCOME TO THIS PROGRAM  ****");
        long programStart = ElapsedTime.systemTime();
        TestsSummary.initTestsSumary(sufixFileOutput + File.separator + "testsSummary.txt");
        /* 1. GET THE LIST OF TESTS TO RUN FORM "test2run.txt"
              aTest = instanceName + testParameters */
        String testsFilePath = testFolder + File.separator + fileNameTest;
        ArrayList<Test> testsList = TestsManager.getTestsList(testsFilePath);
        // 1.1 GET THE LIST OF sustainability Cost TO RUN FROM "test2run.txt"
        String parametersFilePath = inputFolder + File.separator + sufixFileParameter;
        Costs costsList = new Costs(parametersFilePath);
        /* 2. FOR EACH TEST (instanceName + testParameters) IN THE LIST... */
        int nTests = testsList.size();
        System.out.println("number of test "+nTests);
        for( int k = 0; k < nTests; k++ )
        {   Test aTest = testsList.get(k);
            System.out.println("\n# STARTING TEST " + (k + 1) + " OF " + nTests);
            // 2.1 GET THE INSTANCE INPUTS (DATA ON NODES AND VEHICLES)
            // "instanceName_input_nodes.txt" contains data on nodes
            String inputNodesPath = inputFolder + File.separator +aTest.getInstanceName() + sufixFileNodes;
            String inputVehPath = inputFolder + File.separator +aTest.getInstanceName() + sufixFileVehicules;
            String inputTimePath = inputFolder + File.separator + aTest.getInstanceName() + sufixFileTimes;

            // Read inputs files (nodes) and construct the inputs object
            Inputs inputs = InputsManager.readInputs(inputNodesPath, inputVehPath);
            double[][] timeMatrix = InputsManager.readTimes(inputs,inputTimePath);
            InputsManager.generateDepotEdges(inputs, costsList, timeMatrix, aTest);
            InputsManager.generateSavings(inputs, costsList, timeMatrix, aTest);
            
            // 2.2. USE THE MULTI-START ALGORITHM TO SOLVE THE INSTANCE
            Random rng = new Random();
            IGAlgorithm algorithm = new IGAlgorithm(aTest, inputs, rng, costsList);
            Outputs output = algorithm.solve();
            
            // 2.3. PRINT OUT THE RESULTS TO FILE "instanceName_seed_outputs.txt"
            String outputsFilePath = outputFolder + File.separator +
                   aTest.getInstanceName() + "_" + aTest.getSeed() + "_" + aTest.getiTime()+ "_" + aTest.getiDist()+"_" + aTest.getiSocial() +sufixFileOutput;
            output.sendToFile(outputsFilePath);
        }

        /* 3. END OF PROGRAM */
        System.out.println("\n****  END OF PROGRAM, CHECK OUTPUTS FILES  ****");
            long programEnd = ElapsedTime.systemTime();
            System.out.println("Total elapsed time = "
                + ElapsedTime.calcElapsedHMS(programStart, programEnd));
    }
}