import java.util.LinkedList;
import java.util.Random;

public class VNS {
	public static Solution ShakeSolution(Solution Sol, Costs costList,Random rng,Test aTest, Inputs inputs) {
		Sol.updateSolution(costList, aTest);
		Solution currentSol=new Solution(Sol);
		boolean balanced=false;
		int iter=0;
		double elapsed=0;
	    while(iter<11 || !balanced){
	    for (int P=1; P<4;P++){
		 	iter++;
		 	long start = ElapsedTime.systemTime();
	        /* 3. Iterates calls to RandCWS */
		 	currentSol.updateSolution(costList, aTest);
			Solution diffSol=new Solution(currentSol);
			float destPercentage=0;
			switch (P) {
			case 1:
				P=1;
				destPercentage=	aTest.getmaxParamDest();
				break;
			case 2:
				P=2;
				destPercentage= aTest.getmedParamDest();
				break;
			case 3:
				P=3;
			    destPercentage= aTest.getminParamDest();
			    break;}
			System.out.println("destPercentage   "+ destPercentage);
			double nRoutes=(destPercentage*Sol.getRoutes().size());
			diffSol=destructionConstruction(nRoutes,rng,diffSol,costList,aTest,inputs);
			diffSol.updateSolution(costList,aTest);
			diffSol.LocalSearch(aTest, inputs, rng, costList, true);
			balanced=diffSol.checkBalanceConditions(inputs, costList);
			elapsed = ElapsedTime.calcElapsed(start, ElapsedTime.systemTime());
			if(currentSol.getOptimizationCost()>diffSol.getOptimizationCost() && balanced==true) {
				currentSol=diffSol;}}
	    currentSol.LocalSearch(aTest, inputs, rng, costList, true);
	    currentSol.updateSolution(costList,aTest);}
		
		
		
		
		currentSol.setElapsedTime(elapsed);
		return currentSol;}

	private static Solution destructionConstruction( double nRoutes,Random rng, Solution diffSol, Costs costList, Test aTest, Inputs inputs) {
	    double routeToremove=Math.round(nRoutes);
		Solution subproblem= new Solution();
		int extractedRoutes=0;
		while(extractedRoutes<routeToremove){
			int randomRoute=(int)(Math.random()*diffSol.getRoutes().size());
			subproblem.getRoutes().add(diffSol.getRoutes().get(randomRoute));
			diffSol.getRoutes().remove(diffSol.getRoutes().get(randomRoute));
			extractedRoutes++;}
		LinkedList<Node> nodesSubproblem= extractnodes(subproblem);
		subproblem=RandCWS.partialconstruction(aTest,inputs, nodesSubproblem,rng, costList,true);
		for(Route r:subproblem.getRoutes()){
			diffSol.getRoutes().add(r);}
		diffSol.updateSolution(costList, aTest);
		return diffSol;
	}


	public static LinkedList<Node> extractnodes(Solution sol) {
		LinkedList<Node> nodes =new  LinkedList<Node>();
		Node depot = sol.getRoutes().getFirst().getEdges().get(0).getOrigin();
		nodes.add(0,depot);
		for(Route r:sol.getRoutes()){
			for(int e=1;e<=r.getEdges().size()-1;e++){
				nodes.add(r.getEdges().get(e).getOrigin());}}
		return nodes;}

}
