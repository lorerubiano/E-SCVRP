

import java.util.*;

public class RouteCache {

    private final int MAX_CACHE_CAPACITY = 5000000;
    private LinkedHashMap<Set<Node>,Route> cache;
    int hits = 0;
    int update = 0;


    public RouteCache(){
        cache = new LinkedHashMap<>();
    }

    public Route improveRoute(Route route){
        Route cachedRoute = cache.get(route.getNodes());
        if(cachedRoute == null ||cachedRoute.getOptimizationCost() > route.getOptimizationCost()){
            if(cachedRoute == null && cache.size() < MAX_CACHE_CAPACITY) {
                cache.put(route.getNodes(), route);
                
                System.out.println("cache "+cachedRoute==null);
                if(cache.size() >= MAX_CACHE_CAPACITY){
                    System.out.println("FULL!");
                }
            }
            else{
                ++update;
            }


            return route;
        }
        else{
            ++hits;
            return cachedRoute;
        }
    }

    void removeRandomRoute(){

    }




}
