public class Edge implements Comparable<Edge> {
    private Node origin, end;
    private double optimizationCost = 0.0;
    private double distance = 0.0, distCost = 0.0;
    private double time, timeCost = 0.0;
    private double socialCost = 0.0;
    private double savings = 0.0;
    private Route inRoute = null; //route containing this edge
    private Edge inverseEdge = null; //edge with inverse direction

    public Edge(Node originNode, Node endNode) {
        origin = originNode;
        end = endNode;
        distance = distance(origin, end); }

    public void setDist(double dist) {this.distance = dist;}
    public void setOptimizationCost(double c) {optimizationCost = c;}
    public void setSocialCost(double s) {socialCost = s;}
    public void setTime(double time) {this.time = time;}
    public void setInRoute(Route r) {inRoute = r;}
    public void setInverse(Edge e) {inverseEdge = e;}
    public Node getOrigin() {return origin;}
    public Node getEnd() {return end;}
    public double getOptimizationCost() {return optimizationCost;}
    public double getDist() { return distance;}
    public double getDistCost() { return distance;}
    public double getTime() {return time;}
    public double getTimeCost()  {return timeCost;}
   public double getSocial() {return socialCost;}
    public double getSavings() {return savings;}
    public Route getInRoute() {return inRoute;}
    public Edge getInverseEdge() {return inverseEdge;}

    /**
     * Calculates the optimization cost of the edge.
     *
     * @param cost Costs object of the problem
     * @param test Test object of the instance
     */
    public void calcOptimizationCosts(Costs cost, Test test) {
        double costs = 0;
        if (test.getiDist()) {
            calcDistCost(cost.getgEnvironmental(),cost.getKpl(), cost.getDistCost());
            costs += distCost;}
        if (test.getiTime()) {
            calcTimeCost(cost.getgEconomic(),cost.getDriverCost());
            costs += timeCost;}
        if (test.getiSocial()) {
            calcSocialCost(cost.getgSocial(), cost.getSocialCost());
            costs += socialCost;}
        
        this.optimizationCost = costs;}

    public void calcDistCost(double ImportanceWeigth, double kpl, double distCost) {
        this.distCost =  this.distance * kpl * distCost ;}

    public void calcTimeCost(double ImportanceWeigth, double DW) {
        this.timeCost = this.time * DW;}

    public void calcSocialCost(double ImportanceWeigth, double social) {
        double idemand = origin.getDemand();
        double jdemand = end.getDemand();
        this.socialCost = (idemand + jdemand) / 2 * this.distance * social;}

    /**
     * Calculates the savings of the edge.
     * @param depot Depot node
     * @param cost Costs object of the problem
     * @param timeOriginDepot Time from origin to depot
     * @param timeEndDepot Time from end to depot
     * @param test Test object of the instance
     */

    public void calcSavings(Node depot, Costs cost, double timeOriginDepot, double timeEndDepot, Test test) {
        double costOE = this.optimizationCost;
        Edge DO = new Edge(depot, this.origin);
        Edge ED = new Edge(this.end, depot);
        DO.setTime(timeOriginDepot);
        ED.setTime(timeEndDepot);
        DO.calcOptimizationCosts(cost, test);
        ED.calcOptimizationCosts(cost, test);
        double costDO = DO.optimizationCost;
        double costED = ED.optimizationCost;
        this.savings = costDO + costED - costOE;}
    
    /** Edges can be compared by they savings */
    public int compareTo(Edge otherEdge) {
        Edge other = otherEdge;
        double s1 = this.getSavings();
        double s2 = other.getSavings();
        if (s1 < s2) return -1;
        if (s1 > s2) return 1;
        return 0;}

    /**
     * Calculates euclidian distance
     *
     * @param o Node origin
     * @param e Node end
     * @return Euclidian distance o-e
     */
    private double distance(Node o, Node e) {
        double X1 = o.getX();
        double Y1 = o.getY();
        double X2 = e.getX();
        double Y2 = e.getY();
        return Math.sqrt((X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1));
    }
}
