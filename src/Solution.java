import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

/**
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class Solution
{
	/* INSTANCE FIELDS & CONSTRUCTOR */
	private static final String NEW_LINE = System.getProperty("line.separator");
	private static long nInstances = 0; // number of instances
	private long id;
	private double optimizationCost = 0.0;
	private double totalCost = 0.0;
	private double distance = 0.0;
	private double distanceCost = 0.0;
	private double time = 0.0;
	private double timeCost = 0.0;
	private double socialCost = 0.0;
	private double co2Cost = 0.0;
	private float demand = 0.0F;
	private double maxDist;
	private double maxTime;
	private double minDist;
	private double minTime;
	private LinkedList<Route> routes;
	private double runTime = 0.0; // sec.

	public Solution()
	{   nInstances++;
	id = nInstances;
	routes = new LinkedList<Route>();
	}
	
	public Solution( Solution sol)
	{   nInstances++;
	id = nInstances;
	routes = new LinkedList<Route>();
	for (Route r:sol.getRoutes()){
		Route rSol= new Route(r);
		routes.add(rSol);}
	this.optimizationCost = sol.optimizationCost;
	this.totalCost = sol.totalCost;
	this.distance = sol.distance;
	this.distanceCost = sol.distanceCost;
	this.time = sol.time;
	this.timeCost = sol.timeCost;
	this.socialCost =sol.socialCost;
	this.co2Cost =sol.co2Cost;
	this.demand = sol.demand;
	this.maxDist=sol.maxDist;
	this.maxTime=sol.maxTime;
	this.minDist=sol.minDist;
	this.minTime=sol.minTime;
	this.runTime =sol.runTime; // sec.
	}

	/* GET METHODS */
	public LinkedList<Route> getRoutes() {return routes;}
	public long getId() {return id;}
	public double getOptimizationCost() {return optimizationCost;}
	public double getTotalCost() {return totalCost;}
	public double getDistances() {return distance;}
	public double getDistancesCost() {return distanceCost;}
	public double getTimeRoutes() {return time;}
	public double getTimeRoutesCost() {return timeCost;}
	public double getCO2cost() {return co2Cost;}
	public double getSocialCost() {return socialCost;}
	public float getDemand() {return demand;}
	public double getTime() {return runTime;}
	public void computeMaxMin(){
		setMaximums(); 
		setMinimums();}
	public double getMaxDist() {return maxDist;}
	public double getMinDist() {return minDist;}
	public double getMaxTime() {return maxTime;}
	public double getMinTime() {return minTime;}


	/* COPY A ROUTE*/
	private Route copyRoute(Route route) {
		Route r= new Route();
		LinkedList<Edge> edges =new  LinkedList<Edge>(route.getEdges());
		r.setEdges(edges);
		r.setDemand(route.getDemand());
		r.setTime(route.getTime());
		r.setDists(route.getDist());
		r.setOptimizationCost(route.getOptimizationCost());
		r.setSocialCost(route.getSocialCost());
		r.setTotalCost(route.getCostsTotal());
		return r;}


	/* SET METHODS */
	public void setElapsedTime(double t) {this.runTime=t;}

	public void setMinimums() {
		double minDist=this.getRoutes().getFirst().getDist();
		double minTime=this.getRoutes().getFirst().getTime();
		for(Route r:this.getRoutes()){
			if(minDist>=r.getDist()) minDist= r.getDist();
			if(minTime>=r.getTime()) minTime= r.getTime();}
		this.minTime = minTime; this.minDist = minDist;}

	public void setMaximums() {
		double maxDist=this.getRoutes().getFirst().getDist();
		double maxTime=this.getRoutes().getFirst().getTime();
		for(Route r:this.getRoutes()){
			if(maxDist<=r.getDist()) maxDist= r.getDist();
			if(maxTime<=r.getTime()) maxTime= r.getTime();}
		this.maxTime = maxTime; this.maxDist = maxDist;}

	public boolean checkBalanceConditions(Inputs inputs, Costs cost){
		this.computeMaxMin();
		
		if(this.getRoutes().size() > inputs.getVehNum())return false;
		
		return true;
		}

	public void setOptimizationCost() {
		double optimizationCost=0;
		for (Route r: this.routes) {
			optimizationCost+= r.getOptimizationCost();}
		this.optimizationCost = optimizationCost;}    

	public void setTotalCost(Costs cost, Test test) {
		double costsTotal=0;
		for(Route r: this.routes) {
			costsTotal += r.setTotalCost(cost, test);}
		this.totalCost = costsTotal;}

	public void setDistances() {
		float aux =0;
		for (Route r: this.routes) {
			aux += r.getDist();}
		this.distance = aux;}

	public void setDistancesCost() {
		float aux=0;
		for (Route r: this.routes) {
			aux += r.getDistsCost();}
		this.distanceCost=aux;}
	
	public void setTime() {
		float aux=0;
		for (Route r: this.routes) {
			aux += r.getTime();}
		this.time=aux;}

	
	public void setTimeCost() {
		float aux=0;
		for (Route r: this.routes) {
			aux += r.getTimeCost();}
		this.timeCost=aux;}

	
	public void setSocialCost() {
		float aux = 0;
		for (Route r: this.routes) {
			aux += r.getSocialCost();}
		this.socialCost=aux;}

	public void setDemand(float d) {demand = d;}

	/*  AUXILIARY METHODS */

	@Override


	public String toString()
	{
		Route aRoute; // auxiliary Route variable
		String s = "";
		s = s.concat("\r\n");
		s = s.concat("Sol ID : " + getId() + "\r\n");
		s = s.concat("Sol Optimization costs: " + getOptimizationCost() + "\r\n");
		s = s.concat("Sol Total costs: " + getTotalCost() + "\r\n");
		s = s.concat("Sol Dist " + getDistances() + "\r\n");
		s = s.concat("Sol Dist cost: " + getDistancesCost() + "\r\n");
		s = s.concat("Sol Time: " + getTimeRoutes() + "\r\n");
		s = s.concat("Sol Time cost: " + getTimeRoutesCost() + "\r\n");
		s = s.concat("Sol Social cost: " + getSocialCost() + "\r\n");
		s = s.concat("Sol largest route (Time): " + getMaxTime() + "\r\n");
		s = s.concat("Sol shortest route (Time): " + getMinTime() + "\r\n");
		s = s.concat("Sol largest route (Dist): " + getMaxDist() + "\r\n");
		s = s.concat("Sol shortest route (Dist): " + getMinDist() + "\r\n");
		s = s.concat("Sol computing time: " + runTime + "\r\n");
		s = s.concat("# of routes in sol: " + routes.size());
		s = s.concat("\r\n\r\n\r\n");
		s = s.concat("List of routes (cost and nodes): \r\n\r\n");
		for (int i = 0; i < routes.size(); i++)
		{   aRoute = routes.get(i);
		s = s.concat("Route " + i + " || ");
		s = s.concat("Total costs = " + aRoute.getCostsTotal() + " || ");
		s = s.concat("Optimization costs = " + aRoute.getOptimizationCost() + " || ");
		s = s.concat("Social Cost = " + aRoute.getSocialCost() + " || ");
		s = s.concat("Distance  = " + aRoute.getDist()+ " || ");
		s = s.concat("Distance Cost = " + aRoute.getDistsCost() + " || ");
		s = s.concat("Time  = " + aRoute.getTime()+ " || ");
		s = s.concat("Time cost  = " + aRoute.getTimeCost()+ " || ");
		s = s.concat("Demand  = " + aRoute.getDemand()+ " || ");
		s = s.concat("Nodes  = ");           
		for(Edge e : aRoute.getEdges()) {
			if(e.getOrigin().getId() == 0)
				s = s.concat("0" + "-" + e.getEnd().getId());
			else s = s.concat("-" + e.getEnd().getId());}            
		s = s.concat(NEW_LINE);}        
		return s;}


	public void computeOptimizationCost(Costs costs, Test aTest) {
		optimizationCost = 0.0;
		for(Route r:routes){
			r.setOptimizationCost(costs, aTest);
			optimizationCost += r.getOptimizationCost();}}
	
	

	



	public void updateSolution(Costs costList,Test aTest ) {
		setDistances();
		setDistancesCost();
		setTime();
		setTimeCost();
		setSocialCost();
		setOptimizationCost();
		setTotalCost(costList, aTest);
		}

	public void LocalSearch(Test aTest, Inputs inputs, Random rng, Costs costList, boolean b) {
	for(Route r:this.getRoutes()){
		r.repairRoute(inputs, aTest, costList);}}

	public void fastLocalSearch(Inputs inputs, RouteCache cache, Test aTest, Costs costList) {
		Solution sol= new Solution(this);
		sol.improveUsingCache(cache,inputs,aTest, costList);
		sol.updateSolution(costList, aTest);
		
	}

	private void improveUsingCache(RouteCache cache, Inputs inputs, Test aTest, Costs costList) {
		for(Route rR:this.getRoutes()) {
			rR.repairRoute(inputs, aTest, costList);
			cache.improveRoute(rR);
			rR.updateRoute(costList,aTest);
			
		}
		
	}


	
	


}