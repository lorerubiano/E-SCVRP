import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class Test {
    private String instanceName;
    private float maxRouteCosts; // max cost per route
    private float maxTime;
    private String distrib; // distribution for randomness
    private float firstParam, secondParam,maxP,minP,medP; // parameters of distribution and percentages for routes destruction
    private int seed;
    private boolean iTime, iDistance, iSocial, iCO2;
    public float k, var, penalty;
    public int simC, simL, range;

    public Test(String name, float rCosts, float t, String d, float p1, float p2, float maxpdest, float medPdest, float minpdest,
                int s, boolean iT, boolean iDist, boolean iS) {
        instanceName = name;
        maxRouteCosts = rCosts;
        maxTime = t;
        distrib = d;
        firstParam = p1;
        secondParam = p2;
        maxP=maxpdest;
        medP=medPdest;
        minP=minpdest;
        seed = s;
        iTime = iT;
        iDistance=iDist;
        iSocial=iS;       }

    public String getInstanceName() {
        return instanceName;
    }
    public float getMaxRouteCosts() {
        return maxRouteCosts;
    }
    public float getMaxTime() {
        return maxTime;
    }
    public String getDistribution() {
        return distrib;
    }
    public float getFirstParam() {
        return firstParam;
    }
    public float getSecondParam() {
        return secondParam;
    }
    
    public float getminParamDest() {
        return minP;
    }
    
    public float getmedParamDest() {
        return medP;
    }
    public float getmaxParamDest() {
        return maxP;
    }
    public int getSeed() {
        return seed;
    }
    public boolean getiTime() {
        return iTime;
    }
    public boolean getiDist() {
        return iDistance;
    }
    public boolean getiSocial() {
        return iSocial;
    }
    public boolean getiCO2() {
        return iCO2;
    }
    public float getK() {
        return k;
    }
    public float getVar() {
        return var;
    }
    public int getSimC() {
        return simC;
    }
    public int getSimL() {
        return simL;
    }
    public float getPenalty() {
        return penalty;
    }    
    public int getRange() {
        return range;
    }

	public String getSumary(Outputs output){
		String result = instanceName + "_" +  seed + "_" + iTime  + "_" + iDistance  + "_" + iSocial  + "_" + iCO2;
		if (output == null) result += " No feasible solution was found";
		else result += " DISTANCE: " + output.getOBSol().getDistances();
		return result;}

		
}