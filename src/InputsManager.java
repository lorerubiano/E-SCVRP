import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class InputsManager
{
    public static Inputs readInputs(String nodesFilePath, String vehiclesFilePath)
    {
        Inputs inputs = null;
        try
        {   // 1. COUNT THE # OF NODES (# OF LINES IN nodesFilePath)
            BufferedReader br = new BufferedReader(new FileReader(nodesFilePath));
            String f = null;
            int nNodes = 0;
            while( (f = br.readLine()) != null )
            {   if( f.charAt(0) != '#' )
                    nNodes++;
            }
            // 2. CREATE THE INPUTS OBJECT WITH nNodes
            inputs = new Inputs(nNodes);
            // 3. CREATE ALL NODES AND FILL THE NODES LIST
            FileReader reader = new FileReader(nodesFilePath);
            Scanner in = new Scanner(reader);
            in.useLocale(Locale.US);
            String s = null;
            int k = 0;
            while( in.hasNextLine() )
            {   s = in.next();
                if( s.charAt(0) == '#' ) // this is a comment line
                    in.nextLine(); // skip comment lines
                else
                {   float x = Float.parseFloat(s); 
                    float y = in.nextFloat();
                    float demand = in.nextFloat();
                    Node node = new Node(k, x, y, demand);
                    inputs.getNodes()[k] = node;
                    k++;
                }
            }
            in.close();
         // 4. READ VEHICLE CAPACITY (HOMOGENEOUS FLEET)
            reader = new FileReader(vehiclesFilePath);
            in = new Scanner(reader);
            in.useLocale(Locale.US);
            int nVehInType=0;
            while( in.hasNextLine() )
            {   s = in.next();
                nVehInType++;
                if( s.charAt(0) == '#' ) // this is a comment line
                    in.nextLine(); // skip comment lines
                else
                {   float vCap = Float.parseFloat(s);
                
                	
                	inputs.setVehCap(vCap);
                	inputs.setVehNum(nVehInType);
                }
            }
           
            in.close();
        }
        catch (IOException exception)
        {   System.out.println("Error processing inputs files: " + exception);
        }
        return inputs;
    }

    /**
     * Creates the (edges) savingsList according to the CWS heuristic.
     */
    public static void generateSavings(Inputs inputs, Costs cost, double[][] times, Test test) {
        int nNodes = inputs.getNodes().length;
        Edge[] savingsArray = new Edge[(nNodes - 1) * (nNodes - 2) / 2];
        Node depot = inputs.getNodes()[0];
        int k = 0;
        for (int i = 1; i < nNodes - 1; i++) { // node 0 is the depot
            for (int j = i + 1; j < nNodes; j++) {
                Node iNode = inputs.getNodes()[i];
                Node jNode = inputs.getNodes()[j];
                Edge ijEdge = new Edge(iNode, jNode);
                double timeOriginDepot = times[0][iNode.getId()];
                double timeEndDepot = times[0][jNode.getId()];
                ijEdge.setTime(times[i][j]);
                ijEdge.calcOptimizationCosts(cost, test);
                ijEdge.calcSavings(depot, cost, timeOriginDepot, timeEndDepot, test);
                Edge jiEdge = new Edge(jNode, iNode);
                jiEdge.setTime(times[i][j]);
                jiEdge.calcOptimizationCosts(cost, test);
                jiEdge.calcSavings(depot, cost, timeEndDepot, timeOriginDepot, test);
                ijEdge.setInverse(jiEdge);
                jiEdge.setInverse(ijEdge);
                savingsArray[k] = ijEdge;
                k++;
            }
        }
        // Construct the savingsList by sorting the edgesList. Uses the compareTo()
        //Arrays.sort(savingsArray);
        Arrays.sort(savingsArray,Collections.reverseOrder());
        List sList = Arrays.asList(savingsArray);
        LinkedList savingsList = new LinkedList(sList);
        inputs.setList(savingsList);
    }

    /*
     * Creates the list of paired edges connecting node i with the depot,
     *  i.e., it creates the edges (0,i) and (i,0) for all i > 0.
     */
    public static void generateDepotEdges(Inputs inputs, Costs cost, double[][] times, Test test) {
    	Edge[][] edges = new Edge[inputs.getNodes().length][inputs.getNodes().length];
        //Generate depot edges
    	Node[] nodes = inputs.getNodes();
        Node depot = nodes[0]; // depot is node 0
        for (int i = 1; i < nodes.length; i++) {
            Node iNode = nodes[i];
            Edge diEdge = new Edge(depot, iNode);
            diEdge.setTime(times[0][iNode.getId()]);
            diEdge.calcOptimizationCosts(cost, test);
            iNode.setDiEdge(diEdge);
            edges[0][iNode.getId()] = diEdge;
            Edge idEdge = new Edge(iNode, depot);
            idEdge.setTime(times[0][iNode.getId()]);
            idEdge.calcOptimizationCosts(cost, test);
            iNode.setIdEdge(idEdge);
            edges[iNode.getId()][0] = idEdge;
            // Set inverse edges
            idEdge.setInverse(diEdge);
            diEdge.setInverse(idEdge);
        }
        
      //Generate node to node edges
        for (int i = 0; i < nodes.length; i++) { 
        	for(int j = i+1; j < nodes.length; ++j){
        		Node Node_i = nodes[i];
        		Node Node_j = nodes[j];
        		Edge ijEdge = new Edge(Node_i, Node_j);
        		ijEdge.setTime(times[Node_i.getId()][Node_j.getId()]);
        		ijEdge.calcOptimizationCosts(cost, test);
        		edges[Node_i.getId()][Node_j.getId()] = ijEdge;
               
        		Edge jiEdge = new Edge(Node_j, Node_i);
        		jiEdge.setTime(times[Node_j.getId()][Node_i.getId()]);
        		jiEdge.calcOptimizationCosts(cost, test);
        		edges[Node_j.getId()][Node_i.getId()]= jiEdge;
        		 
        		ijEdge.setInverse(jiEdge);
        		jiEdge.setInverse(ijEdge);
        		}
        	}
       /* for(int e=0;e<edges.length;e++){ 
        	for(int ef = e+1; ef < edges.length; ++ef){
        
        	System.out.println("arc:  ("+edges[e][ef].getOrigin().getId()+ ","+edges[e][ef].getEnd().getId()+ ") Time: "+edges[e][ef].getTime()+"Opt cost "+edges[e][ef].getOptimizationCost());
    		
        	System.out.println("inverse arc:");
    		System.out.println("arc:  ("+edges[e][ef].getInverseEdge().getOrigin().getId()+ ","+edges[e][ef].getInverseEdge().getEnd().getId()+ ") Time: "+edges[e][ef].getInverseEdge().getTime()+"Opt cost "+edges[e][ef].getInverseEdge().getOptimizationCost());
    		System.out.println("next arc:");
        	}}*/
        inputs.setEdgess(edges);
        }
        
    

    /**
    * @return geometric center for a set of nodes
    */
    public static float[] calcGeometricCenter(List<Node> nodesList)
    {
        Node[] nodesArray = new Node[nodesList.size()];
        nodesArray = nodesList.toArray(nodesArray);
        return calcGeometricCenter(nodesArray);
    }

    public static float[] calcGeometricCenter(Node[] nodes)
    {
        // 1. Declare and initialize variables
	float sumX = 0.0F; // sum of x[i]
	float sumY = 0.0F; // sum of y[i]
	float[] center = new float[2]; // center as (x, y) coordinates
	// 2. Calculate sums of x[i] and y[i] for all iNodes in nodes
	Node iNode; // iNode = ( x[i], y[i] )
	for( int i = 0; i < nodes.length; i++ )
	{   iNode = nodes[i];
            sumX = sumX + iNode.getX();
            sumY = sumY + iNode.getY();
	}
	// 3. Calculate means for x[i] and y[i]
	center[0] = sumX / nodes.length; // mean for x[i]
	center[1] = sumY / nodes.length; // mean for y[i]
	// 4. Return center as (x-bar, y-bar)
	return center;
    }

	public static double[][] readTimes(Inputs inputs, String inputTimePath){
		int n=inputs.getNumNodes();
    	double[][] timelist = new double[n][n];
    	for(int i = 0; i < n; i++) timelist[i][i] = 0;
        try{
            FileReader reader = new FileReader(inputTimePath);
            Scanner in = new Scanner(reader);
            in.useLocale(Locale.US);
            String s = null;
            while(in.hasNextLine()){
                s = in.next();
                if(s.charAt(0) == '#') in.nextLine();
                else{
                    int a = Integer.parseInt(s);
                    int b = in.nextInt();
                    double time = in.nextDouble();                    
                    timelist[a][b] = time;
                    timelist[b][a] = time;}}
            in.close();}
        catch(IOException exception){
            System.out.println("Error processing inputs files: " + exception);}
        inputs.setTimes(timelist);
        return timelist;}
}