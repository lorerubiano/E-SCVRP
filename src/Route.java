import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;



public class Route {
    private double optimizationCost = 0.0;
    private double totalCost = 0.0;
    private double distance = 0.0, distCost = 0.0;
    private double time = 0.0, timeCost = 0.0;
    private double socialCost = 0.0;
    private float demand = 0.0F;
    private LinkedList<Edge> edges;
    public Route() {edges = new LinkedList<Edge>();}
    
    
    public Route(Route r) {
    	optimizationCost = r.optimizationCost;
    	totalCost=r.totalCost;
    	demand = r.demand;
    	edges = new LinkedList<Edge>(r.edges);
    	time = r.time;
    	timeCost = r.timeCost;
    	distance = r.distance;
    	distCost = r.distCost;
    	socialCost = r.socialCost;}
    
    
   
    
    
    
    public double getCostsTotal() {return totalCost;}
    public double getOptimizationCost() {return optimizationCost;}
    public double getDist() {return distance;}
    public double getDistsCost() {return distCost;}
    public double getTime() {return time;}
    public double getTimeCost() {return timeCost;}
    public double getSocialCost() {return socialCost;}
    public float getDemand() {return demand;}
    public List<Edge> getEdges() {return edges;}
    public int getSize() {return this.edges.size();}
    
    public void setDists(double d) {distance = d;}
    public void setTime(double t) {time = t;}
    public void setDemand(float q) {demand = q;}
    public void setEdges(LinkedList<Edge> e) {edges = e;}
    public void setOptimizationCost(double c) {this.optimizationCost=c ;}

    public void setOptimizationCost(Costs c, Test test) {
        double costs = 0;
        if(test.getiDist()) {
            calcDistCost(c);
            costs += c.getgEnvironmental()*distCost;}
        if(test.getiTime()) {
            calcTimeCost(c, test);
            costs += c.getgEconomic()*timeCost;}
        if(test.getiSocial()) {
            setSocialCost(c);
            costs += c.getgSocial()*socialCost;}
        this.optimizationCost = costs;}
    
    
    public double setTotalCost(Costs c, Test test) {
        double costs = 0;
            calcDistCost(c);
            costs += distCost;
            calcTimeCost(c, test);
            costs += timeCost;
            setSocialCost(c);
            costs += socialCost;        
        totalCost =  costs;
        return totalCost; }

    public void setSocialCost(Costs c) {
        socialCost = 0;
        double[] demandAcc = new double[edges.size()];
        int i=0;
        for(Edge e:edges) {
            if(i==0) demandAcc[i]= 0;
            else demandAcc[i]=demandAcc[i-1]+e.getOrigin().getDemand();
            i++;}
        double demandaTotal=demandAcc[edges.size()-1];
        for(i=0; i<edges.size(); i++) {
            double Yij = demandaTotal - demandAcc[i];
            socialCost+=c.getSocialCost()*Yij*edges.get(i).getDist();}}
  
    
  
    public void calcTimeCost(Costs c, Test t) {
        timeCost = (time*c.getDriverCost()+c.getfixedCost());}

    public void calcDistCost(Costs c) {distCost=c.getDistCost()*distance*c.getKpl();}
    

  
    /**
     * Reverses a route, e.g. (0 -> 2 -> 6 -> 0) becomes (0 -> 6 -> 2 -> 0)
     */
    public void reverse() {
        for(int i = 0; i < edges.size(); i++) {
            Edge e = edges.get(i);
            Edge invE = e.getInverseEdge();
            edges.remove(e);
            edges.add(0, invE);}}
   
    public Set<Node> getNodes() {
        Set<Node> nodes = new LinkedHashSet<>();
        for(Edge e : edges){
            nodes.add(e.getOrigin());
            nodes.add(e.getEnd());
        }
        return Collections.unmodifiableSet(nodes);
    }

    public LinkedList<Node> extractnodesFromRoute(Route r) {
		LinkedList<Node> nodes =new  LinkedList<Node>();
		for(int e=1;e<=r.getEdges().size()-1;e++){
				nodes.add(r.getEdges().get(e).getOrigin());}
		return nodes;}
	
    
	public void repairRoute(Inputs inputs, Test test, Costs costs) {
		if(this.getEdges().size()>= 4){
			boolean improve = true;
			while(improve){
				improve = false;
				for (int i = 1; i <this.getEdges().size()-1 && !improve; i++){
					for (int j = i + 1; j < this.getEdges().size() && !improve; j++){
						Node ni_1 = this.getEdges().get(i - 1).getOrigin();
						Node ni = this.getEdges().get(i).getOrigin();
						Node nj = this.getEdges().get(j).getOrigin();
						Node njP1 = this.getEdges().get(j).getEnd();
						//Depot depot = new Depot(edges.get(0).getOrigin();
						Edge e1 = this.getEdges().get(i-1);
						Edge e2 = this.getEdges().get(j);
						Edge e1Alternative =  inputs.getEdge(ni_1.getId(),nj.getId());
						Edge e2Alternative =  inputs.getEdge(ni.getId(),njP1.getId());
                        double currentEstimateCost = e1.getOptimizationCost() + e2.getOptimizationCost();
						double alternativeEstimateCost = e1Alternative.getOptimizationCost() + e2Alternative.getOptimizationCost();
                       if(alternativeEstimateCost < currentEstimateCost){
							improve = twoOptSwap(i, j,e1,e2, e1Alternative, e2Alternative,costs,test);
						}
					}
				}
			}
		}
	}
    

    public boolean twoOptSwap(int i, int j,Edge e1, Edge e2, Edge e1Alternative, Edge e2Alternative, Costs costs, Test test){
    	 Route route = new Route(this);
         route.edges.remove(i - 1);
         route.edges.add(i - 1, e1Alternative);
         route.reverse(i, j - 1);
         route.edges.remove(j);
         route.edges.add(j, e2Alternative);
         route.updateRoute(costs,test);
         this.updateRoute(costs,test);
         if(route.getOptimizationCost() < this.optimizationCost){ 
        	 this.optimizationCost = route.optimizationCost;
             this.distCost = route.distCost;
             this.timeCost = route.timeCost;
             this.socialCost = route.socialCost;
             this.time = route.time;
             this.distance = route.distance;
             this.edges = route.edges;
             return true;}
         return false;

     }
      


    public void updateRoute(Costs costs,Test test) {
    	this.demand=0;
    	this.distance=0;
    	this.time=0;
    	for(Edge e : this.getEdges()) {
    		this.setDemand(this.getDemand() + e.getEnd().getDemand());
    		this.setDists(this.getDist()+e.getDist());
    		this.setTime(this.getTime()+e.getTime());}
    	this.calcTimeCost(costs,test);
    	this.calcDistCost(costs);
    	this.setOptimizationCost(costs,test);
    	this.setTotalCost(costs,test);
		
	}


	private void reverse(int init, int end){
        for (int i = init; i <= end; i++) {
            Edge e = edges.get(i);
            Edge invE = e.getInverseEdge();
            edges.remove(i);
            edges.add(init, invE);
        }
    }


	public void setSocialCost(double socialCost2) {
		this.socialCost=socialCost2;
		
	}


	public void setTotalCost(double costsTotal) {
		this.totalCost=costsTotal;
		
	}


	public void setInvEdges() {
		for(Edge e:this.edges){
		Edge ie=new Edge(e.getEnd(),e.getOrigin());
		e.setInverse(ie);
		ie.setInverse(e);}
		
	}
    
	

}