import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**  
 * Biased-randomized version of the Clarke & Wright savings (CWS) heuristic.
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class RandCWS {
    public static Solution solve(Test aTest, Inputs inputs, Random rng, Costs costs, boolean useRandom) {
        // resets isInterior and inRoute in nodes
        Solution currentSol = generateDummySol(inputs,costs,aTest);
        currentSol.computeMaxMin();
        Node depot = inputs.getNodes()[0];
        int index;
        double p1 = aTest.getFirstParam();
        double p2 = aTest.getSecondParam();
        double minRouteTime=1.5*(currentSol.getMaxTime());
        double minRouteDist=1.5*(currentSol.getMaxDist());
        boolean balancedSol = false;
           	List<Edge> savings = new LinkedList<Edge>();
        	for(Edge e : inputs.getSavings()) savings.add(0, e); // Copy savingsList in reverse order
        	while(!savings.isEmpty()) {
	            if(!useRandom) index = 0;// CWS
	            else index = getRandomPosition(p1,p2, rng, savings.size()); // BRCWS
	            Edge ijEdge = savings.get(index);
	            savings.remove(ijEdge); // remove edge from list
	            // 3.2. Determine the nodes i < j that define the edge
	            Node iNode = ijEdge.getOrigin();
	            Node jNode = ijEdge.getEnd();
	            // 3.3. Determine the routes associated with each node
	            Route iR = iNode.getInRoute();
	            Route jR = jNode.getInRoute();
	            // 3.4. If all necessary conditions are satisfied, merge
	            boolean isMergePossible;
	            isMergePossible = checkMergingConditions(aTest, inputs, iR, jR, ijEdge,costs,minRouteTime,minRouteDist);
	            if(isMergePossible) {
	            	// 3.4.1. Get an edge iE in iR containing nodes i and 0
	                Edge iE = getEdge(iR, iNode, depot); // iE is either (0,i) or (i,0)
	                // 3.4.2. Remove edge iE from iR route and update costs
	                iR.getEdges().remove(iE);
	                iR.setDists(iR.getDist()-iE.getDist());
	                iR.setTime(iR.getTime()-iE.getTime());	             
	                // 3.4.3. If there are more than one edge then i will be interior
	                if( iR.getEdges().size() > 1 )
	                    iNode.setIsInterior(true);
	                // 3.4.4. If new route iR does not start at 0 it must be reversed
	                if( iR.getEdges().get(0).getOrigin() != depot )
	                    iR.reverse();
	                // 3.4.5. Get an edge jE in jR containing nodes j and 0
	                Edge jE = getEdge(jR, jNode, depot); // jE is either (0,j) or (j,0)
	                // 3.4.6. Remove edge jE from jR route
	                jR.getEdges().remove(jE);
	                jR.setDists(jR.getDist()-jE.getDist());
	                jR.setTime(jR.getTime()-jE.getTime());
	                // 3.4.7. If there are more than one edge then j will be interior
	                if(jR.getEdges().size() > 1) jNode.setIsInterior(true);
	                // 3.4.8. If new route jR starts at 0 it must be reversed
	                if(jR.getEdges().get(0).getOrigin() == depot) jR.reverse(); // reverseRoute(inputs, jR);
	                // 3.4.9. Add ijEdge = (i, j) to new route iR
	                iR.getEdges().add(ijEdge);
	                iR.setDists(iR.getDist()+ijEdge.getDist());
	                iR.setTime(iR.getTime()+ijEdge.getTime());
	                iR.setDemand(iR.getDemand() + ijEdge.getEnd().getDemand());
	                jNode.setInRoute(iR);
	                // 3.4.10. Add route jR to new route iR
	                for(Edge e : jR.getEdges()) {
	                    iR.getEdges().add(e);
	                    iR.setDemand(iR.getDemand() + e.getEnd().getDemand());
	                    iR.setDists(iR.getDist()+e.getDist());
	                    iR.setTime(iR.getTime()+e.getTime());
	                    e.getEnd().setInRoute(iR);}
	                // 3.4.11. Delete route jR from currentSolution
	                currentSol.getRoutes().remove(jR);}
	         // 3.4.12. Update largest and shortest route from currentSolution
	            currentSol.computeMaxMin();}
        	balancedSol = currentSol.checkBalanceConditions(costs);
        	System.out.println(balancedSol);
        currentSol.computeOptimizationCost(costs, aTest);
        currentSol.localsearch(currentSol,inputs,costs,aTest);
        //currentSol.computeOptimizationCost(costs, aTest);
        
        if(aTest.getiDist()){
            currentSol.setDistances();
            currentSol.setDistancesCost();}
        if(aTest.getiTime()) {
            currentSol.setTime();
            currentSol.setTimeCost();
        }
        if(aTest.getiSocial()) currentSol.setSocialCost();
        return currentSol;
    }
    
    

	/** 
     * Constructs an initial dummy feasible solution as described in the CWS
     *  heuristic: dummySol = { (0,i,0) / i in vrpNodesList }
     * During this process, inRoute and isInterior values are assigned.
     */
    private static Solution generateDummySol(Inputs inputs,Costs costList,Test aTest) {
        Solution sol = new Solution();
        for(int i = 1; i < inputs.getNodes().length; i++) { // i = 0 is the depot
            Node iNode = inputs.getNodes()[i];
            Edge diEdge = iNode.getDiEdge();
            Edge idEdge = iNode.getIdEdge();
            Route didRoute = new Route();
            didRoute.getEdges().add(diEdge);
            didRoute.setDemand(didRoute.getDemand() + diEdge.getEnd().getDemand());
            didRoute.setDists(didRoute.getDist() + diEdge.getDist());
            didRoute.setTime(didRoute.getTime() + diEdge.getTime());
            didRoute.setOptimizationCost(costList,aTest);
            didRoute.setTotalCost(costList,aTest);
            didRoute.getEdges().add(idEdge);
            didRoute.setDemand(didRoute.getDemand() + idEdge.getEnd().getDemand());
            didRoute.setDists(didRoute.getDist() + idEdge.getDist());
            didRoute.setTime(didRoute.getTime() + idEdge.getTime());
            didRoute.setOptimizationCost(costList,aTest);
            didRoute.setTotalCost(costList,aTest);
            iNode.setInRoute(didRoute);
            iNode.setIsInterior(false);
            sol.getRoutes().add(didRoute);
            sol.setDemand(sol.getDemand() + didRoute.getDemand());
        }
        sol.setTotalCost(costList,aTest);
        sol.setOptimizationCost();
        if(aTest.getiDist()) {
            sol.setDistances();
            sol.setDistancesCost();
        }
        if(aTest.getiTime()) {
            sol.setTime();
            sol.setTimeCost();
        }
        if(aTest.getiSocial()) sol.setSocialCost();
        return sol;
    }

    
  
    	
    
    /** 
     * Given aRoute, iNode and depot, returns the edge in aRoute which
     * contains iNode and depot (it will be the first of the last edge)
     */
    private static Edge getEdge(Route aRoute, Node iNode, Node depot)
    {   
        // Check if firstEdge in aRoute contains iNode and depot
        Edge firstEdge = aRoute.getEdges().get(0);
        Node origin = firstEdge.getOrigin();
        Node end = firstEdge.getEnd();
        if ( ( origin == iNode && end == depot )
                || ( origin == depot && end == iNode ) )
            return firstEdge;
        else
        {   int lastIndex = aRoute.getEdges().size() - 1;
            Edge lastEdge = aRoute.getEdges().get(lastIndex);
            return lastEdge;
        }
    }

    private static boolean checkMergingConditions(Test aTest, Inputs inputs,
            Route iR, Route jR, Edge ijEdge, Costs costList, double maxTime, double maxDist) {
        // Condition 1: iR and jR are not the same route
        if( iR == jR ) return false;
        // Condition 2: both nodes are exterior nodes in their respective routes
              Node iNode = ijEdge.getOrigin();
        Node jNode = ijEdge.getEnd();
        if( iNode.getIsInterior()  || jNode.getIsInterior()  ) return false;
        // Condition 3: demand after merging can be covered by a single vehicle
        if( inputs.getVehCap() < iR.getDemand() + jR.getDemand() ){
        	return false;
        }            
        // Condition 4: if the traveling time exceeds the driving range battery life
        if(costList.getbattery() < iR.getTime() + jR.getTime()){
        	return false;
        }
       // Condition 5: if the traveling time exceeds the driving range working hours
        if(costList.getworkingh() < iR.getTime() + jR.getTime()){
        	return false;
        }
        
        if((iR.getDist() + jR.getDist())>maxDist)
        	return false;
        
        if((iR.getTime() + jR.getTime()) >maxTime)
        	return false;
        
        return true;}
   
   

    private static int getRandomPosition(double a, double b, Random r, int size) {
        double beta = a + r.nextDouble() * (b - a);
        int index = (int) (Math.log(r.nextDouble()) / Math.log(1 - beta));
        index = index % size;
        return index;
    }
  
 
}