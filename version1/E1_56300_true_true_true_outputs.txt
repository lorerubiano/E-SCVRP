***************************************************
*                      OUTPUTS                    *
***************************************************


--------------------------------------------

 OUR BEST SOLUTION:

--------------------------------------------

Sol ID : 106440610
Sol Optimization costs: 278.46510690544676
Sol Total costs: 278.46510690544676
Sol Dist 976.1705932617188
Sol Dist cost: 226.6667938232422
Sol Time: 15.329999923706055
Sol Time cost: 151.95030212402344
Sol Social cost: 360.1499938964844
Sol largest route (Time): 11.56
Sol shortest route (Time): 7.800000000000001
Sol largest route (Dist): 702.4264387747186
Sol shortest route (Dist): 483.7555227617158
Sol computing time: 0.237585745
# of routes in sol: 3


List of routes (cost and nodes): 

Route 0 || Total costs = 67.42317745827418 || Optimization costs = 67.42317745827418 || Social Cost = 74.637975730227 || Distance  = 303.2404477915688 || Distance Cost = 70.41243197720227 || Time  = 4.53 || Time cost  = 44.902300000000004 || Demand  = 4485.0 || Nodes  = 0-10-8-3-16-22-0
Route 1 || Total costs = 92.41975976215105 || Optimization costs = 92.41975976215105 || Social Cost = 125.29359153869947 || Distance  = 294.5022106345291 || Distance Cost = 68.38341330933767 || Time  = 4.67 || Time cost  = 46.289699999999996 || Demand  = 2854.0 || Nodes  = 0-9-4-6-1-2-14-17-18-0
Route 2 || Total costs = 118.6221696850215 || Optimization costs = 118.6221696850215 || Social Cost = 160.21844354816102 || Distance  = 378.427905698263 || Distance Cost = 87.87095970313668 || Time  = 6.13 || Time cost  = 60.7583 || Demand  = 2850.0 || Nodes  = 0-12-11-13-7-5-21-19-20-15-0


